---
author: Cyril I.
title: Tableau de bord - Gestion de Classe
---

<style>
.controls {
    position: relative; /*pour que le absolute du titre fonctionne*/
    border: 2px solid white;
    padding: 30px 15px 15px 15px ;
    border-radius: 5px;
    margin-bottom: 40px;
}

.titre {
    position: absolute; /*position absolue par rapport à son parent*/
    top: 0px; /* pour que ça se mette en haut*/
    transform: translateY(-50%); /*pour centrer verticalement autour de cette position*/
    margin: 0 !important; /*Pour que ça se place bien en haut avec la propriété top*/
    padding: 0px 5px 0px 5px !important; /*5px de bleu de chaque côté du titre qui masque la bordure*/
    background-color: #6495ed; /*La même couleur que le fond de page*/
    color: white;
}
</style>

## Gestion de classe


<div class="controls">
<h3 class="titre"><i class="fa fa-plus-square" aria-hidden="true"></i> Créer une grille</h3>
<input type="number" id="width" placeholder="Largeur en px" min="1" max="100">
<input type="number" id="height" placeholder="Hauteur en px" min="1" max="100">
<button onclick="createGrid()">Créer</button>
</div>
<div class="controls">
<h3 class="titre"><i class="fa fa-folder-open" aria-hidden="true"></i> Charger une grille HTML</h3>
<input type="file" id="htmlLoader" accept=".html">
<button onclick="loadHTML()">Charger</button>
</div>
