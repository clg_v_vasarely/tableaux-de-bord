<style>

body {
    display: flex;
    justify-content: center;
}

.container {
    display: flex;
    justify-content: space-between;
    width: 90%;
    padding: 10px;
    border: 2px solid #00FFFF; /* Bleu néon */
    box-shadow: 0 0 10px #00FFFF; /* Effet néon */
    margin: 20px 0;
}

.container h3 {
    width: 100%;
    text-align: center;
}

.container .column {
    width: 30%;
    padding: 10px;
    box-sizing: border-box;
}

.container .column:nth-child(1) {
    margin-right: 2.5%;
}

.container .column:nth-child(2) {
    margin: 0 2.5%;
}

.container .column:nth-child(3) {
    margin-left: 2.5%;
}
</style>

# Bienvenue sur notre espace de Tableaux de Bord Thématiques

Sur cet espace, nous mettons à votre disposition des **tableaux de bord thématiques**. Ces tableaux de bord sont conçus pour vous fournir des applications et des ressources utiles partagées sur LaForgeEdu. Chaque tableau de bord est dédié à un domaine spécifique pour faciliter l'accès à l'information et aux outils nécessaires.

## Nos Tableaux de Bord Thématiques

### Le Tableau de Bord Gestion De Classe
Ce tableau de bord rassemble des outils et des ressources pour aider les enseignants dans la gestion quotidienne de leur classe. Vous y trouverez des applications pour la planification des cours, le suivi des élèves, et bien plus encore.

#### TuxTimer <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M15.91 13.34l2.636-4.026-.454-.406-3.673 3.099c-.675-.138-1.402.068-1.894.618-.736.823-.665 2.088.159 2.824.824.736 2.088.665 2.824-.159.492-.55.615-1.295.402-1.95zm-3.91-10.646v-2.694h4v2.694c-1.439-.243-2.592-.238-4 0zm8.851 2.064l1.407-1.407 1.414 1.414-1.321 1.321c-.462-.484-.964-.927-1.5-1.328zm-18.851 4.242h8v2h-8v-2zm-2 4h8v2h-8v-2zm3 4h7v2h-7v-2zm21-3c0 5.523-4.477 10-10 10-2.79 0-5.3-1.155-7.111-3h3.28c1.138.631 2.439 1 3.831 1 4.411 0 8-3.589 8-8s-3.589-8-8-8c-1.392 0-2.693.369-3.831 1h-3.28c1.811-1.845 4.321-3 7.111-3 5.523 0 10 4.477 10 10z"/></svg>

![Copie d'éran de TuxTimer](IMG/tuxtimes_img.png)

Application de Timer pour donner un temps limiter en classe.

#### Qui Corrige <svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path d="M6 3.447h-1v-1.447h19v16h-7.731l2.731 4h-1.311l-2.736-4h-1.953l-2.736 4h-1.264l2.732-4h-2.732v-1h8v-1h3v1h3v-14h-17v.447zm2.242 17.343c-.025.679-.576 1.21-1.256 1.21-.64 0-1.179-.497-1.254-1.156l-.406-4.034-.317 4.019c-.051.656-.604 1.171-1.257 1.171-.681 0-1.235-.531-1.262-1.21l-.262-6.456-.308.555c-.241.437-.8.638-1.265.459-.404-.156-.655-.538-.655-.951 0-.093.012-.188.039-.283l1.134-4.098c.17-.601.725-1.021 1.351-1.021h4.096c.511 0 1.012-.178 1.285-.33.723-.403 2.439-1.369 3.136-1.793.394-.243.949-.147 1.24.217.32.396.286.95-.074 1.297l-3.048 2.906c-.375.359-.595.849-.617 1.381-.061 1.397-.3 8.117-.3 8.117zm-5.718-10.795c-.18 0-.34.121-.389.294-.295 1.04-1.011 3.666-1.134 4.098l1.511-2.593c.172-.295.623-.18.636.158l.341 8.797c.01.278.5.287.523.002 0 0 .269-3.35.308-3.944.041-.599.449-1.017.992-1.017.547.002.968.415 1.029 1.004.036.349.327 3.419.385 3.938.043.378.505.326.517.022 0 0 .239-6.725.3-8.124.033-.791.362-1.523.925-2.061l3.045-2.904c-.661.492-2.393 1.468-3.121 1.873-.396.221-1.07.457-1.772.457h-4.096zm16.476 1.005h-5v-1h5v1zm2-2h-7v-1h7v1zm-15.727-4.994c-1.278 0-2.315 1.038-2.315 2.316 0 1.278 1.037 2.316 2.315 2.316s2.316-1.038 2.316-2.316c0-1.278-1.038-2.316-2.316-2.316zm0 1c.726 0 1.316.59 1.316 1.316 0 .726-.59 1.316-1.316 1.316-.725 0-1.315-.59-1.315-1.316 0-.726.59-1.316 1.315-1.316zm15.727 1.994h-7v-1h7v1z"/></svg>

Application

#### Digiscreen

Le tableau interactif de La Digitale


### Le Tableau de Bord Ressources en Maths
Un espace dédié aux ressources mathématiques, incluant des exercices, des tutoriels, des vidéos éducatives, et des applications interactives pour renforcer les compétences en mathématiques de vos élèves.

### Le Tableau de Bord Ressources en LaTeX
Découvrez une multitude de ressources pour maîtriser LaTeX. Ce tableau de bord comprend des guides, des modèles de documents, et des astuces pour optimiser votre utilisation de LaTeX dans la rédaction de vos documents académiques.

### Le Tableau de Bord Ressources en NSI
Pour les enseignants et les étudiants en Numérique et Sciences Informatiques (NSI), ce tableau de bord offre des ressources pédagogiques, des projets pratiques, des cours en ligne, et des outils de programmation pour approfondir vos connaissances en informatique.

---

Nous espérons que ces tableaux de bord thématiques vous seront utiles dans votre pratique pédagogique et vous aideront à enrichir vos cours. N'hésitez pas à explorer les différentes sections et à partager vos propres ressources pour contribuer à la communauté de LaForgeEdu.

