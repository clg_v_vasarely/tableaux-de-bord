# Ecrire des Maths sur le Web

Bienvenue sur la page du projet **Ecrire des Maths sur le Web**, une ressource destinée à aider les enseignants et autres utilisateurs à intégrer des formules mathématiques dans des documents web à l'aide de Tex / LaTeX et de la librairie MathJax.

## Accès au Site

Le site est accessible à l'adresse suivante : [Ecrire des Maths sur le Web](https://lmdbt.forge.apps.education.fr/lmdbt/ecrire-des-maths-sur-le-web). Vous y trouverez des tutoriels, des exemples, et des guides étape par étape pour vous initier et vous perfectionner.

## Pourquoi utiliser Tex / LaTeX avec MathJax ?

Tex / LaTeX est un langage de balisage largement utilisé pour la composition de documents scientifiques et mathématiques de haute qualité. MathJax est une librairie JavaScript qui permet de rendre les notations mathématiques écrites en LaTeX directement dans les navigateurs web.

En apprenant à les utiliser, vous pourrez :
- Écrire des formules mathématiques claires et précises sur le web.
- Améliorer la qualité visuelle de vos documents éducatifs.
- Rendre vos supports de cours plus interactifs et accessibles en ligne.

## Ressources supplémentaires

Nous vous proposons une sélection de ressources pour approfondir vos compétences :
- [Zeste De Savoir - Des Mathématiques](https://zestedesavoir.com/tutoriels/826/introduction-a-latex/1322_completer-vos-documents/5376_des-mathematiques/)
- Le [site officiel de MathJax](https://www.mathjax.org/) et sa [documentation](http://docs.mathjax.org/)

## Licence

Ce projet est mis à disposition sous une licence Creative Commons Attribution (CC-BY).
